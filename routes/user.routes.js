const express = require('express');
const { userById, allUsers, getUser, updateUser, deleteUser, addFollowing, addFollowers, removeFollowing, removeFollowers, userPhotoProfile, findPeople, searchUsers, deleteFollower } = require('../controllers/user.controller');
const { requireSignin } = require('../controllers/auth');

const router = express.Router();
// Follow unfollow
router.put('/user/follow', requireSignin, addFollowing, addFollowers);
router.put('/user/unfollow', requireSignin, removeFollowing, removeFollowers);
router.put('/delete/follower', requireSignin, deleteFollower);

router.get('/users', requireSignin, allUsers);
router.get('/user/:userId', requireSignin, getUser);
router.put('/user/:userId', requireSignin, updateUser);
router.get('/searchusers', requireSignin, searchUsers);

router.delete('/user/:userId', requireSignin, deleteUser);

// photo profile
router.get("/user/photo/:userId", userPhotoProfile);

// who to follow
router.get('/user/findpeople/:userId', requireSignin, findPeople);


// any route containing : userid, our app will first execute userById
router.param('userId', userById);

module.exports = router;
