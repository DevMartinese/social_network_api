const express = require('express');
const { userById } = require('../controllers/user.controller');
const { signout, signin, signup, socialLogin, forgotPassword, resetPassword } = require('../controllers/auth');
// import password reset validator
const { userSignupValidator, passwordResetValidator } = require("../validator");

const router = express.Router();
 
// password forgot and reset routes
router.put("/forgot-password", forgotPassword);
router.put("/reset-password", passwordResetValidator, resetPassword);

// then use this route for social login
router.post("/social-login", socialLogin);

router.post('/auth/signup', userSignupValidator, signup);
router.post('/auth/signin', signin);
router.get('/auth/signout', signout);
// any route containing : userid, our app will first execute userById
router.param('userId', userById);

module.exports = router;