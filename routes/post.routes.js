const express = require('express');
const { userById } = require('../controllers/user.controller');
const { getPosts,createPost, postByUser, postById, deletePost, isPoster, updatePost, singlePost, like, unLike, commentPost, unCommentPost, postsForFollowings } = require('../controllers/post.controller');
const { requireSignin } = require('../controllers/auth');
const { createPostValidator } = require('../validator/index');

const router = express.Router();

router.get('/posts', requireSignin, getPosts);
// Like
router.put('/post/like', requireSignin, like);
router.put('/post/unlike', requireSignin, unLike);
// Comments
router.put('/post/comment', requireSignin, commentPost);
router.put('/post/uncomment', requireSignin, unCommentPost);

router.get('/post/:postId', requireSignin, singlePost);
router.get('/posts/by/:userId', requireSignin, postByUser);
router.post('/createpost/:userId', requireSignin, createPost, createPostValidator);
router.put('/updatepost/:postId', requireSignin, isPoster, updatePost);
router.delete('/deletepost/:postId', requireSignin, isPoster, deletePost);

router.get('/posts/users/me/:userId', requireSignin, postsForFollowings);


// any route containing : userid, our app will first execute userById()
router.param('userId', userById);

// any route containing : userid, our app will first execute postById()
router.param('postId', postById);

module.exports = router;