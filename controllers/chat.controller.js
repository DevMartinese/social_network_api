const Chat = require('../models/Chat');

exports.getAllChats = async (req, res) => {
    const chats = await Chat.find()
        .populate('sender', '_id name photo_profile')
        .sort({ created: -1 })

    res.json(chats)
}
