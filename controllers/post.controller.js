const Post = require('../models/post');
const formidable = require('formidable');
const fs = require('fs');
const _ = require('lodash');

exports.postById = (req, res, next, id) => {
    Post.findById(id)
        .populate('postedBy', '_id name photo_profile') // would replace the include in sequelize
        .populate('comments', 'text created')
        .populate('comments.postedBy', '_id name photo_profile')
        .exec((err, post) => { // Lake then
            if (err || !post) {
                return res.status(400).json({ error: err });
            }
            req.post = post;
            next();
        })
}

exports.getPosts = (req, res) => {
    const posts = Post.find()
        .select('_id title body photo postedBy likes') // selecting only the fields i return
        .populate('postedBy', '_id name photo_profile')
        .populate('comments', 'text created')
        .populate('comments.postedBy', '_id name photo_profile')
        .sort({ created: -1 })
        .then(posts => {
            res.json({ posts })
        })
        .catch(err => console.log(err));
}

exports.postsForFollowings = async (req, res) => {
    let following = req.profile.following;
    following.push(req.profile._id);

    // get current page from req.query or use default value of 1
    const currentPage = req.query.page || 1;
    // return 3 posts per page
    const perPage = 6;

    // countDocuments() gives you total count of posts
    const totalItems = await Post.find({ postedBy: { $in: req.profile.following } }).countDocuments();

    Post.find({ postedBy: { $in: req.profile.following } })
        .skip((currentPage - 1) * perPage)
        .populate('comments', 'text created')
        .populate('comments.postedBy', '_id name photo_profile')
        .populate('postedBy', '_id name photo_profile')
        .sort('-created')
        .limit(perPage)
        .exec((err, posts) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler.getErrorMessage(err)
                })
            }
            res.json({ totalItems, posts })
        })

}

exports.singlePost = (req, res) => {
    return res.json(req.post);
}

exports.createPost = async (req, res) => {
    const post = await new Post(req.body);
    post.postedBy = req.profile;
    await post.save(post);
    res.json(post);
}

exports.postByUser = async (req, res) => {
    // get current page from req.query or use default value of 1
    const currentPage = req.query.page || 1;
    // return 3 posts per page
    const perPage = 5;


    // countDocuments() gives you total count of posts
    const totalItems = await Post.find({ postedBy: req.profile._id }).countDocuments();

    Post.find({ postedBy: req.profile._id })
        .skip((currentPage - 1) * perPage)
        .populate('postedBy', '_id name photo_profile') // would replace the include in sequelize
        .sort({ created: -1 })
        .limit(perPage)
        .exec((err, posts) => { // Lake then
            if (err) {
                return res.status(400).json({ error: err });
            }
            res.json({ totalItems, posts });
        })
}

exports.isPoster = (req, res, next) => {
    let isPoster = req.post && req.auth && req.post.postedBy._id == req.auth._id;

    if (!isPoster) {
        return res.status(403).json({ error: "User is not authorized" });
    }

    next();
}

exports.deletePost = (req, res) => {
    let post = req.post;
    post.remove((err, post) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json({ message: "Post deleted successfully" });
    });
}

exports.updatePost = (req, res) => {
    let post = req.post;
    post = _.extend(post, req.body)
    post.updated = Date.now();
    post.save(err => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json(post);
    });
}

exports.like = (req, res, next) => {
    const { postId, userId } = req.body;
    Post.findByIdAndUpdate(postId,
        { $push: { likes: userId } },
        { new: true }
    ).exec((err, result) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json(result);
    });
}

exports.unLike = (req, res, next) => {
    const { postId, userId } = req.body;
    Post.findByIdAndUpdate(postId,
        { $pull: { likes: userId } },
        { new: true }
    ).exec((err, result) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json(result);
    });
}

exports.commentPost = (req, res) => {
    const { comment, postId, userId } = req.body;
    comment.postedBy = userId;

    Post.findByIdAndUpdate(postId,
        { $push: { comments: comment } },
        { new: true }

    )
        .populate('comments.postedBy', '_id name')
        .populate('postedBy', '_id name')
        .exec((err, result) => {
            if (err) {
                return res.status(400).json({ error: err });
            }
            res.json(result);
        });
}

exports.unCommentPost = (req, res) => {
    const { postId, comment } = req.body;

    Post.findByIdAndUpdate(postId,
        { $pull: { comments: { _id: comment._id } } },
        { new: true }
    ).exec((err, result) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json(result);
    });
}

