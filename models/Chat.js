const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;
const chatSchema = new mongoose.Schema({
    message: {
        type: String
    },
    sender: {
        type: ObjectId,
        ref: "User"
    },
    type: {
        type: String
    }
}, { timestamps: true });

module.exports = mongoose.model('Chat', chatSchema);